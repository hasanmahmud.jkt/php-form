<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        .body{width:900px; margin:0 auto;}
        .form {width:700px;}
        .col-md-2 h1 {font-size:20px;color:#00fafa;text-align: center; }

    </style>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body class="body">

        <div class="container">
            <div class="row">

                <div class="col-md-2"> <h1>Registration form</h1> </div>
                <div class="col-md-8">

               <form class="form" action="regoutput.php" method="post">
                   <table class="table tablebordered">

                       <tr>
                           <td> Enter your Name </td>
                           <td>
                               <div class="form-group">
                                   <input type="text" name="Fullname" class="form-control" id="exampleInputName" aria-describedby="emailHelp" placeholder="Enter name">
                               </div>
                           </td>
                       </tr>
                       <tr>
                           <td> Enter your Email  </td>
                           <td>
                               <div class="form-group">
                                   <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                               </div>
                           </td>
                       </tr>
                       <tr>
                           <td> Enter your password </td>
                           <td>
                               <div class="form-group">
                                   <input type="password" name="password" class="form-control" id="exampleInputPassword"  placeholder="Enter password">
                               </div>
                           </td>
                       </tr>
                       <tr>
                           <td> Date of Birth </td>
                           <td>
                               <?php

                               echo '<select name="day">';
                               echo '<option>Day</option>';
                               for($i = 1; $i <= 31; $i++){
                                   $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                                   echo "<option value='$i'>$i</option>";
                               }
                               echo '</select>';

                               echo '</select>/';
                               echo '<select name="month">';
                               echo '<option>Month</option>';
                               for($i = 1; $i <= 12; $i++){
                                   $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                                   echo "<option value='$i'>$i</option>";
                               }
                               echo '</select>/';

                               echo '<select name="year">';
                               echo '<option>Year</option>';
                               for($i = date('Y'); $i >= date('Y', strtotime('-100 years')); $i--){
                                   echo "<option value='$i'>$i</option>";
                               }

                               ?>

                           </td>
                       </tr>
                       <tr>
                           <td>Gender</td>
                           <td>
                               <input type="radio" name="gender" value="Male" > Male
                               <input type="radio" name="gender" value="Female" > Female
                               <input type="radio" name="gender" value="Other" > Other

                           </td>
                       </tr>
                       <tr>
                           <td>Choose your Profile Picture</td>
                           <td>
                               <input type="file" name="picture" class="form-control-file" id="exampleFormControlFile1">
                           </td>
                       </tr>
                       <tr>
                           <td>
                               <input class="btn btn-primary" type="submit" value="Submit">
                               <input class="btn btn-primary" type="reset" value="Reset">
                           </td>
                       </tr>
                   </table>
               </form>
                </div>
            </div>
        </div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>